# Declare intrinsic variables.
data "aws_region" "current" {}
locals {
  region = data.aws_region.current.name
}

variable "api_name" {
  type        = string
  description = "The name of the REST API."
  #  sensitive   = true
  #  validation {
  #    condition     = length(var.ami) > 4 && substr(var.ami, 0, 4) == "ami-"
  #    error_message = "Please provide a valid value for variable AMI."
  #  }
}

variable "api_description" {
  type        = string
  description = "A description for the REST API."
}

variable "stage_name" {
  type        = string
  description = "The name of the API stage to deploy to."
}

variable "use_waf" {
  type        = bool
  default     = false
  description = "Whether to use a Web Application Firewall in front of the API."
}

variable "cors" {
  type        = string
  default     = "*"
  description = "API CORS configuration."
}

variable "validate_parameters" {
  type        = bool
  default     = false
  description = "Whether to validate request parameteres."
}

variable "validate_body" {
  type        = bool
  default     = false
  description = "Whether to validate request body."
}

variable "endpoint_config" {
  type        = list(string)
  default     = ["REGIONAL"]
  description = "API endpoint type. Valid values are [\"REGIONAL\"], [\"EDGE\"], or [\"PRIVATE\"]."
}

variable "vpc_id" {
  type        = string
  default     = null
  description = "VPC ID for private API."
}

variable "vpc_endpoint_sgids" {
  type        = list(string)
  default     = null
  description = "A list of Security Group IDs to attach to private API."
}

variable "vpc_endpoint_subnet_ids" {
  type        = list(string)
  default     = null
  description = "A list of Subnet IDs to attach to private API."
}
