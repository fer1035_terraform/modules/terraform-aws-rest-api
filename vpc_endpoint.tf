resource "aws_vpc_endpoint" "vpc_endpoint" {
  count = element(var.endpoint_config, 0) == "PRIVATE" ? 1 : 0

  vpc_id            = var.vpc_id
  subnet_ids        = var.vpc_endpoint_subnet_ids
  service_name      = "com.amazonaws.${local.region}.execute-api"
  vpc_endpoint_type = "Interface"

  security_group_ids = var.vpc_endpoint_sgids

  private_dns_enabled = element(var.endpoint_config, 0) == "PRIVATE" ? true : false
}

resource "aws_api_gateway_rest_api_policy" "api_policy" {
  count = element(var.endpoint_config, 0) == "PRIVATE" ? 1 : 0

  rest_api_id = aws_api_gateway_rest_api.api.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": "*",
      "Action": "execute-api:Invoke",
      "Resource": [
        "execute-api:/*"
      ]
    },
    {
      "Effect": "Deny",
      "Principal": "*",
      "Action": "execute-api:Invoke",
      "Resource": [
        "execute-api:/*"
      ],
      "Condition" : {
        "StringNotEquals": {
          "aws:SourceVpce": "${aws_vpc_endpoint.vpc_endpoint[0].id}"
        }
      }
    }
  ]
}
EOF
}
